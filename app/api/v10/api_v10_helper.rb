# -*- coding: UTF-8 -*-

require 'doorkeeper/grape/helpers'

module ApiV10Helper
  def set_api_header
    header 'Access-Control-Allow-Origin','*'
    header 'Access-Control-Request-Method', 'GET, POST, PUT, OPTIONS, HEAD'
    header 'Access-Control-Allow-Headers', 'x-requested-with,Content-Type, Authorization'    
  end

  def doorkeeper_render_error_with(error)
    case error.reason
    when :unknown
      status_code =401
      code = "e41101"
    when :expired
      status_code =403
      code = "e41100"
    when :revoked
      status_code =401
      code = "e41102"
    end
    error!(message_json(code), status_code)
  end

  def current_user
    target_user = User.where(id: doorkeeper_token.resource_owner_id).first if doorkeeper_token
    error!(message_json("e40004"), 404) unless target_user
    target_user
  end

  def not_user_token!
    error!(I18n.t("doorkeeper.errors.messages.invalid_token.unknown"),400) unless doorkeeper_token.user.blank?
  end





  def message_json code
    {
      code: code,
      message: I18n.t("api.#{code}")
    }
  end

end